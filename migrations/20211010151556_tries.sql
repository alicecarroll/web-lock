-- Add migration script here
CREATE TABLE IF NOT EXISTS tries (
    id          INTEGER PRIMARY KEY NOT NULL,
    password    TEXT                NOT NULL
);
