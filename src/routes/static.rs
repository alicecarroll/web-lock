use actix_web::{get, Responder};
use tracing::instrument;

#[instrument]
#[get("/js/validate.js")]
async fn validate_js() -> impl Responder {
    include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/static/js/validate.js"
    ))
}
