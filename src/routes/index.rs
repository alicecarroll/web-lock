use super::{validate_password, AppState, LockState};
use crate::{config::Config, webhook::Notification};
use actix_web::{web, Responder, ResponseError};
use http::StatusCode;
use serde::Deserialize;
use std::ops::Deref;
use thiserror::Error;
use tracing::{debug, debug_span, instrument};
use tracing_futures::Instrument;

#[derive(Deserialize, Debug)]
pub(crate) struct QueryData {
    /// Either user's guess or [`None`]
    pub password: Option<String>,
}

#[derive(askama::Template, Debug)]
#[template(path = "index.html", print = "all")]
struct Template {
    /// Previous tries
    tries:  Vec<String>,
    /// Current lock state
    state:  LockState,
    /// Config
    config: Config,
}

#[instrument]
pub(crate) async fn handle(
    data: web::Data<AppState>,
    query: web::Query<QueryData>,
) -> Result<impl Responder, Error> {
    let mut tries: Vec<String> = sqlx::query!("SELECT password FROM tries ORDER BY id DESC")
        .fetch_all(&data.pool)
        .instrument(debug_span!("SELECTing previous tries"))
        .await?
        .into_iter()
        .map(|i| i.password)
        .collect();
    if LockState::from_tries(
        &(tries.iter().map(String::as_str).collect::<Vec<&str>>()),
        &data.config.password,
        data.config.limit,
    )
    .enabled()
    {
        async {
            let mut con = data
                .pool
                .acquire()
                .instrument(debug_span!("acquiring database connection"))
                .await?;
            if let Some(ref pwd) = query.password {
                if !validate_password(pwd) {
                    return Err(Error::InvalidPassword(pwd.clone()));
                }
                if !tries.contains(pwd) {
                    sqlx::query!(
                        r#"
INSERT INTO tries ( password )
VALUES ( ?1 )
                "#,
                        pwd
                    )
                    .execute(&mut con)
                    .instrument(debug_span!("inserting password", password = ?pwd))
                    .await?;
                    tries.push(pwd.clone());
                    data.client
                        .post(data.config.webhook_url.clone())
                        .json(&Notification {
                            pwd,
                            state: LockState::from_tries(
                                &(tries.iter().map(String::as_str).collect::<Vec<&str>>()),
                                &data.config.password,
                                data.config.limit,
                            ),
                            tries: (data.config.limit - tries.len(), data.config.limit),
                        })
                        .send()
                        .instrument(debug_span!("send_notification"))
                        .await?;
                }
            }
            Ok::<_, Error>(())
        }
        .instrument(debug_span!("handling user provided password"))
        .await?;
    }
    let state = LockState::from_tries(
        &(tries.iter().map(String::as_str).collect::<Vec<&str>>()),
        &data.config.password,
        data.config.limit,
    );
    debug!(?tries, ?state);

    Ok(Template {
        tries,
        state,
        config: data.config.clone(),
    })
}

#[allow(variant_size_differences)]
#[derive(Error, Debug)]
pub(crate) enum Error {
    #[error(transparent)]
    Sqlx(#[from] sqlx::Error),
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),
    #[error("password {0:?} is invalid")]
    InvalidPassword(String),
}

impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        match self {
            Self::InvalidPassword(_) => StatusCode::BAD_REQUEST,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
