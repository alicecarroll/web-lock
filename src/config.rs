use serde::Deserialize;
use url::Url;

#[derive(Debug, Deserialize, Clone)]
pub(crate) struct Config {
    /// Correct password
    pub password:    String,
    /// Amount of tries after which the lock is disabled forever
    pub limit:       usize,
    /// Discord webhook URL
    pub webhook_url: Url,
    /// Socket to bind to
    pub bind_to:     String,
}
