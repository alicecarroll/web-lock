use super::LockState;
use serde::Serialize;
use serde_json::json;
use tracing::{debug, instrument};

#[derive(Debug, Serialize, Clone)]
#[serde(into = "serde_json::Value")]
pub(crate) struct Notification<'a> {
    pub pwd:   &'a str,
    pub state: LockState,
    pub tries: (usize, usize),
}

impl From<Notification<'_>> for serde_json::Value {
    #[instrument]
    fn from(n: Notification<'_>) -> Self {
        let title = match n.state {
            LockState::Disabled => "Замок заблокирован",
            LockState::Unlocked => "Замок открыт",
            LockState::Locked => "Попытка открыть замок",
        };
        let color = match n.state {
            LockState::Disabled => 0xCC241D,
            LockState::Unlocked => 0x98971A,
            LockState::Locked => 0xD79921,
        };
        let j = json!({
            "embeds": [
                {
                    "title": title,
                    "color": color,
                    "footer": {
                        "text": "Made by Alice Carroll#3344 (ID: 426757590022881290)",
                        "icon_url": "https://cdn.discordapp.com/avatars/426757590022881290/dad9dd483259579205f963cc9d22a8c1.png",
                    },
                    "fields": [
                        {
                            "name": "Введённый код",
                            "value": format!("`{}`", n.pwd),
                            "inline": true,
                        },
                        {
                            "name": "Осталось попыток",
                            "value": format!("{left}/{total}", left=n.tries.0, total=n.tries.1),
                            "inline": true,
                        }
                    ],
                },
            ],
        });
        debug!(json=%j);
        j
    }
}
