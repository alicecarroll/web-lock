// https://gist.github.com/GoldsteinE/1e0ee30166004ca6c35b29884679a327
// {{{ lint me harder
#![forbid(non_ascii_idents)]
#![forbid(unsafe_code)]
#![deny(keyword_idents)]
#![deny(elided_lifetimes_in_paths)]
#![deny(rust_2021_incompatible_closure_captures)]
#![deny(rust_2021_incompatible_or_patterns)]
#![deny(rust_2021_prefixes_incompatible_syntax)]
#![deny(unused_crate_dependencies)]
#![warn(explicit_outlives_requirements)]
#![warn(macro_use_extern_crate)]
#![warn(meta_variable_misuse)]
#![warn(noop_method_call)]
#![warn(pointer_structural_match)]
#![warn(rust_2021_prelude_collisions)]
#![warn(semicolon_in_expressions_from_macros)]
#![warn(single_use_lifetimes)]
#![warn(trivial_numeric_casts)]
#![warn(unused_lifetimes)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::wildcard_dependencies)]
#![warn(clippy::pedantic)]
#![warn(clippy::clone_on_ref_ptr)]
#![warn(clippy::create_dir)]
#![warn(clippy::debug_assert_with_mut_call)]
#![warn(clippy::decimal_literal_representation)]
#![warn(clippy::empty_line_after_outer_attr)]
#![warn(clippy::exit)]
#![warn(clippy::get_unwrap)]
#![warn(clippy::indexing_slicing)]
#![warn(clippy::lossy_float_literal)]
#![warn(clippy::option_if_let_else)]
#![warn(clippy::panic)]
#![warn(clippy::rc_buffer)]
#![warn(clippy::rc_mutex)]
#![warn(clippy::redundant_field_names)]
#![warn(clippy::rest_pat_in_fully_bound_structs)]
#![warn(clippy::shadow_unrelated)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_lit_as_bytes)]
#![warn(clippy::string_to_string)]
#![warn(clippy::unneeded_field_pattern)]
#![warn(clippy::unwrap_used)]
#![warn(clippy::useless_let_if_seq)]
#![allow(clippy::missing_errors_doc)]
// my additions
#![allow(clippy::unreadable_literal)]
#![allow(clippy::non_ascii_literal)]
// }}}

use actix_web::{web, App, HttpServer};
use color_eyre::{Report, Result};
use dotenv::dotenv;
use kiam::when;
use reqwest::Client;
use sqlx::sqlite::SqlitePool;
use std::env;
use tracing::{debug, info, info_span, instrument};
use tracing_actix_web::TracingLogger;
use tracing_futures::Instrument;
use tracing_subscriber::{fmt::format::FmtSpan, EnvFilter};
#[cfg(feature = "static_files")]
use {actix_files as fs, std::path::PathBuf};

mod config;
mod routes;
mod webhook;
use self::config::Config;

#[actix_web::main]
async fn main() -> Result<()> {
    setup()?;
    info!("Finished setup!");
    let config: Config = envy::from_env()?;
    let bind_to = config.bind_to.clone();
    debug!(?config);
    assert!(
        validate_password(&config.password),
        "correct password must be valid",
    );
    let pool = async {
        let db_url = env::var("DATABASE_URL")?;
        debug!(DATABASE_URL = ?db_url);
        let pool = SqlitePool::connect(&db_url)
            .instrument(info_span!(
                "sql.connect",
                uri = ?db_url,
                "connecting to SQLite database",
            ))
            .await?;
        sqlx::migrate!()
            .run(&pool)
            .instrument(info_span!("sql.migrate", "running migrations"))
            .await?;
        Result::<_, Report>::Ok(pool)
    }
    .instrument(info_span!("sql.setup", "sql setup"))
    .await?;
    let server = info_span!("web.server.create", "creating HTTP server").in_scope(|| {
        let data = web::Data::new(AppState {
            pool,
            client: Client::new(),
            config,
        });
        debug!(app_data = ?data);
        HttpServer::new(move || {
            let _span = info_span!("web.app.create", "creating app");
            let app = App::new()
                .wrap(TracingLogger::default())
                .app_data(data.clone())
                .route("/", web::to(routes::index::handle));

            info_span!("web.app.add_service.static", "adding static files service").in_scope(|| {
                #[cfg(not(feature = "static_files"))]
                {
                    info!("`static_files` feature disabled, using included static files");
                    app.service(routes::r#static::validate_js)
                }
                #[cfg(feature = "static_files")]
                {
                    info!("`static_files` feature enabled, using filesystem service");
                    app.service(
                        fs::Files::new(
                            "/",
                            dbg!(PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("static")),
                        )
                        .show_files_listing(),
                    )
                }
            })
        })
    });

    server
        .bind(&bind_to)?
        .run()
        .instrument(info_span!("running server"))
        .await?;
    Ok(())
}

fn setup() -> Result<()> {
    dotenv().ok();

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1");
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }
    tracing_subscriber::fmt::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_ansi(true)
        .with_target(true)
        .with_level(true)
        .with_thread_names(true)
        .with_thread_ids(true)
        .with_env_filter(EnvFilter::from_default_env())
        .pretty()
        .init();

    Ok(())
}

#[derive(Debug)]
struct AppState {
    /// SQLite pool
    pool:   SqlitePool,
    /// reqwest client
    client: Client,
    /// Config
    config: Config,
}

#[derive(Debug, Clone)]
enum LockState {
    /// Lock can be unlocked with the correct password
    Locked,
    /// Lock is locked forever 😭
    Disabled,
    /// Lock was unlocked with the correct password 🎉
    Unlocked,
}

impl LockState {
    /// The lock can be unlocked
    fn enabled(&self) -> bool {
        matches!(self, Self::Locked)
    }

    /// Lock state from users' guesses, correct password and guess limit
    #[instrument]
    fn from_tries(tries: &[&str], pwd: &str, limit: usize) -> Self {
        when! {
            tries.len() > limit => Self::Disabled,
            tries.iter().any(|i| *i == pwd) => Self::Unlocked,
            tries.len() == limit => Self::Disabled,
            _ => Self::Locked,
        }
    }
}

/// Check if password is valid
#[instrument]
fn validate_password(pwd: &str) -> bool {
    pwd.len() == 6 && pwd.chars().all(|i| i.is_ascii_digit())
}
