# Running

1. Create the database ([`sqlx-cli`][sqlx-cli] is able to do that):

   ```sh
   cargo sqlx db create
   ```

1. Compile and run the program:

   ```sh
   cargo run -- 123456 5
   ```

[sqlx-cli]: https://github.com/launchbadge/sqlx/tree/master/sqlx-cli
